const csvtojson = require("./csvToJsonConvert");
const fs = require("fs");
const matchesPath = "../data/matches.csv";
const deliveryPath = "../data/deliveries.csv";

function playersDismissedByPlayer(){
    csvtojson(matchesPath).then((matches)=>{
        csvtojson(deliveryPath).then((deliveries)=>{

            let newObject = {};
        //iterating through deliveries data
        for (let object of deliveries) {
          let batsman = object.batsman;
          let bowler = object.bowler;
          // console.log(batsman);
          let dismissalKind = object.dismissal_kind;

          //if dismissalKind is true the storing the batsman and bowler
          if (dismissalKind) {
            if (newObject.hasOwnProperty(batsman)) {
              if (newObject[batsman].hasOwnProperty(bowler)) {
                newObject[batsman][bowler] += 1;
              } else {
                newObject[batsman][bowler] = 1;
              }
            } else {
              newObject[batsman] = {};
              newObject[batsman][bowler] = 1;
            }
          }
        }

        //getting the keys of newObject into array
        let keysArray = Object.keys(newObject);
        let valuesArray = Object.values(newObject);
        //   console.log(keysArray)
        let finalObject = {};
        //iterating the bowlers and finding who dismissed more times
        for (let key of keysArray) {
          let randomValue = 0;
          let randomKey;
          for (let innerKey in newObject[key]) {
            if (newObject[key][innerKey] > randomValue) {
              randomValue = newObject[key][innerKey];
              randomKey = innerKey;
            }
          }
          finalObject[key] = {
            bowler: randomKey,
            occurs: randomValue,
          };
        }
        fs.writeFile("../public/output/playersDismissedByPlayer.json", JSON.stringify(finalObject, null, 1), (error)=>{
            if(error){
                console.log("error")
            }
            else{
                console.log("sucessfully created file in outputPath")
            }
        })
        
        })
    })
}
playersDismissedByPlayer();