const csvtojson = require("./csvToJsonConvert");
const fs = require("fs");

const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/topEconomicalBowler.json';

function topEconomicalBowler() {
    csvtojson(matchesPath).then((matches) => {
        csvtojson(deliveriesPath).then((deliveries) => {


            let bowlerWithRuns = {};
            let totalDeliveries = {}
            for (let match of matches) {
                for (let delivery of deliveries) {
                    if (match.season === "2015" && match.id === delivery.match_id) {
                        if (bowlerWithRuns[delivery.bowler]) {
                            bowlerWithRuns[delivery.bowler] += Number(delivery.total_runs)
                            totalDeliveries[delivery.bowler]++;
                        }
                        else {
                            bowlerWithRuns[delivery.bowler] = Number(delivery.total_runs)
                            totalDeliveries[delivery.bowler] = 1;
                        }

                    }
                }
            }
            let bowlerRunsKey = Object.keys(bowlerWithRuns);
            let strikeRate = {};
            for (let key of bowlerRunsKey) {
                strikeRate[key] = parseFloat((bowlerWithRuns[key] / totalDeliveries[key]) * 6);
            }
            console.log(strikeRate);
            let finalStrikeRate = Object.keys(strikeRate).sort((a, b) => {
                return strikeRate[a] - strikeRate[b];
            }).slice(0, 10);

            fs.writeFile(outputPath, JSON.stringify(finalStrikeRate, null, 2), (error)=>{
                if (error) {
                    console.log("Error in the your data");
                } else {
                    console.log("JSON file has been created at Output Path");
                }
        
            })



        })

    })
}
topEconomicalBowler()