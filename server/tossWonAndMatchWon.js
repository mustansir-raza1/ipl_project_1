const csvtojson = require("./csvToJsonConvert.js");
const fs = require("fs");
const matchesPath = "../data/matches.csv";
const outputPath = '../public/output/tossWonAndMatchWon.json';

function tossWonAndMatchWon() {
  try {
    csvtojson(matchesPath).then((matches) => {
      const teamTOWinBothTossAndMatch = {};
      for (match of matches) {
        if (match.toss_winner == match.winner) {
          if (teamTOWinBothTossAndMatch.hasOwnProperty(match.winner)) {
            teamTOWinBothTossAndMatch[match.winner] ++;
          } else {
            teamTOWinBothTossAndMatch[match.winner] = 1;
          }
        }
      }
     
      fs.writeFile(
        outputPath,
        JSON.stringify(teamTOWinBothTossAndMatch, null, 1),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("successfully created the output file");
          }
        }
      );
    });
  } catch (error) {
    console.log(error);
  }
}

tossWonAndMatchWon();

