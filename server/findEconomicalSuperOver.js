const csvtojson = require("./csvToJsonConvert.js");
const fs = require("fs");
const deliveriesPath = "../data/deliveries.csv";
function findBestBowlerInSuperOvers() {
  try {
    csvtojson(deliveriesPath).then((deliveries) => {
      const bowlersData = {};

      for (delivery of deliveries) {
        // looping through all deliveries to find the all the bowlers who bowled in a super over
        if (delivery.is_super_over == "1") {
          if (!bowlersData.hasOwnProperty(delivery.bowler)) {
            // checking if the bowler is present in bowlersData or not
            bowlersData[delivery.bowler] = {
              totalBallsBowled: 0,
              totalRunsConceded: 0,
            }; // if not adding the bowler in the bowlersdata.
          }

          if (delivery.wide_runs == 0 && delivery.noball_runs == 0) {
            // checking if the delivery is good ball or not
            bowlersData[delivery.bowler].totalBallsBowled++;
          }
          bowlersData[delivery.bowler].totalRunsConceded +=
            Number(delivery.total_runs) - Number(delivery.legbye_runs);
        }
      }

      for (bowler in bowlersData) {
        // looping through bowlersdata to calculate each bowler economy;
        let noOfOverBowled = bowlersData[bowler].totalBallsBowled / 6;
        let economy = bowlersData[bowler].totalRunsConceded / noOfOverBowled; // economy = total runs / no of overs
        bowlersData[bowler] = economy;
      }

      let bestBowlerInSuperOvers = [];
      let economy = Infinity;

      for (bowler in bowlersData) {
        // looping through bowlersdata to find the best bowler in super overs.
        if (economy == bowlersData[bowler]) {
          bestBowlerInSuperOvers.push(bowler);
        } else if (economy > bowlersData[bowler]) {
          economy = bowlersData[bowler];
          bestBowlerInSuperOvers = [];
          bestBowlerInSuperOvers.push(bowler);
        }
      }
      const bestBowlerInSuperOver = {
        bowlers: bestBowlerInSuperOvers,
        economy: economy,
      };
      // console.log(bestBowlerInSuperOver);
      fs.writeFile(
        "../public/output/bestBowlerInsuperOvers.json",
        JSON.stringify(bestBowlerInSuperOver),
        (error) => {
          if (error) {
            console.log(error);
          } else {
            console.log("successfully created the output file");
          }
        }
      );
    });
  } catch (error) {
    console.log(error);
  }
}

findBestBowlerInSuperOvers();

