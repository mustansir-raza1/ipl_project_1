const getData = require("./csvToJsonConvert.js");
const fs = require("fs");
const matchesFile = "../data/matches.csv";
const deliveriesFile = "../data/deliveries.csv";

function findExtraRunsConcededPerTeam(year) {
  
    getData(matchesFile).then((matches) => {
      getData(deliveriesFile).then((deliveries) => {

        const extraRunsConcededPerTeam = {};
        
        for (match of matches) {
          if (match.season == year) {
            for (delivery of deliveries) {
              if (delivery.match_id == match.id) {
                if (
                  extraRunsConcededPerTeam.hasOwnProperty(delivery.bowling_team)
                ) {
                  extraRunsConcededPerTeam[delivery.bowling_team] += Number(
                    delivery.extra_runs
                  );
                } else {
                  extraRunsConcededPerTeam[delivery.bowling_team] = Number(
                    delivery.extra_runs
                  );
                }
              }
            }
          }
        }
        fs.writeFile(
          "../public/output/extraRunsConceded.json",
          JSON.stringify(extraRunsConcededPerTeam, null, 2),
          (err) => {
            if (err) {
              throw new Error("could not write the file");
            } else {
              console.log("successfully created the output file");
            }
          }
        );
      });
    });
  
}

findExtraRunsConcededPerTeam("2016");

