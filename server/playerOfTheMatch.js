const getData = require("./csvToJsonConvert.js");
const fs = require("fs");
const matchesPath = "../data/matches.csv";

function findPlayerWithMostMomAwards() {
  try {
    getData(matchesPath).then((matches) => {
      const ManOfTheMatchAwards = {};

      for (match of matches) {
        if (ManOfTheMatchAwards.hasOwnProperty(match.season)) {
          // chcking if the manofthematchawards has the current season in it or not.
          if (
            ManOfTheMatchAwards[match.season].hasOwnProperty(
              match.player_of_match
            )
          ) {
            // checking if the player already got MOM awards in the current season or not
            ManOfTheMatchAwards[match.season][match.player_of_match]++; // if he already got increment his awards
          } else {
            ManOfTheMatchAwards[match.season][match.player_of_match] = 1; // if he didn't get initailize with 1
          }
        } else {
          ManOfTheMatchAwards[match.season] = { [match.player_of_match]: 1 }; // if the season is not present then add the season along with the player and awards count.
        }
      }

      const playersWithMostManOftheMatchAwards = {};
      for (season in ManOfTheMatchAwards) {
        //looping through the seasons
        let count = 0;
        let players = [];
        for (player in ManOfTheMatchAwards[season]) {
          // looping through the list of players who won MOM awards in that particular season
          if (count == ManOfTheMatchAwards[season][player]) {
            players.push(player);
          } else if (count < ManOfTheMatchAwards[season][player]) {
            count = ManOfTheMatchAwards[season][player];
            players = [];
            players.push(player);
          }
        }
        playersWithMostManOftheMatchAwards[season] = {
          players: players,
          "Man of the match award received": count,
        };
      }
      // console.log(playersWithMostManOftheMatchAwards);
      fs.writeFile(
        "../public/output/playersWithMostManOftheMatchAwards.json",
        JSON.stringify(playersWithMostManOftheMatchAwards, null, 1),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("successfully created the output file");
          }
        }
      );
    });
  } catch (error) {
    console.log(error);
  }
}

  findPlayerWithMostMomAwards();

