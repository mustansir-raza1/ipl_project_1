const csvtojson = require("./csvToJsonConvert.js");
const fs = require("fs");
const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";

function findStrikeRate(batsmanName) {
  try {
    csvtojson(matchesPath).then((matches) => {
      csvtojson(deliveriesPath).then((deliveries) => {
        const batsmanData = {};

        for (match of matches) {
          if (!batsmanData.hasOwnProperty(match.season)) {
            // checking if the season is already present in the batsmandata.
            batsmanData[match.season] = {
              totalRunsScored: 0,
              totalBallsFaced: 0,
            }; // if not adding the season to the batsmandata.
          }
          for (delivery of deliveries) {
            if (match.id == delivery.match_id) {
              if (delivery.batsman == batsmanName) {
                if (delivery.wide_runs == "0") {
                  // checking if it is goodball or not
                  batsmanData[match.season].totalBallsFaced++;
                }
                batsmanData[match.season].totalRunsScored += Number(
                  delivery.batsman_runs
                );
              }
            }
          }
        }
        const batsmanStrikeRate = { name: batsmanName };

        for (year in batsmanData) {
          // looping through batsmandata to calculate the strike rate
          if (
            batsmanData[year].totalRunsScored == 0 &&
            batsmanData[year].totalRunsScored == 0
          ) {
            batsmanStrikeRate[year] = 0;
          } else {
            let strikeRate =
              batsmanData[year].totalRunsScored /
              batsmanData[year].totalBallsFaced;
            batsmanStrikeRate[year] = strikeRate * 100;
          }
        }
        fs.writeFile(
          "../public/output/strikeRateOfABatsman.json",JSON.stringify(batsmanStrikeRate),
          (error) => {
            if (error) {
              console.log(error);
            } else {
              console.log("successfully created the output file");
            }
          }
        );
      });
    });
  } catch (error) {
    console.log(error);
  }
}

findStrikeRate("RG Sharma");

