const fs = require('fs');
const getJSONData = require('./csvToJsonConvert');

const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/matchesTeamPerYear.json';


function matchesTeamPerYear() {
    getJSONData(matchesPath).then((data) => {
        if (Array.isArray(data)) {
            let matchesTeamPerYear = {}
            for (let index = 0; index < data.length; index++) {
                let newSeason = data[index].season;
                let winner = data[index].winner;

                if (winner == "") {
                    continue;
                }

                if (matchesTeamPerYear[newSeason]) {

                    if (matchesTeamPerYear[newSeason][winner]) {
                        matchesTeamPerYear[newSeason][winner] += 1;
                    }
                    else {
                        matchesTeamPerYear[newSeason][winner] = 1;
                    }
                }
                else{
                    matchesTeamPerYear[newSeason] = {}
                    matchesTeamPerYear[newSeason][winner] = 1;
                }

            }
            fs.writeFile(outputPath, JSON.stringify(matchesTeamPerYear, null, 2), (error) => {
                if (error) {
                    console.log("Error in the your data");
                } else {
                    console.log("JSON file has been created at Output Path");
                }
            })

        }
        else {
            return "";
        }

    });
}
matchesTeamPerYear();